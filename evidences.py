import os
import json
from collections import defaultdict

def parse_evidences_dataset():
    evidences_directory = "sourceId=eva/"
    evidences_data = defaultdict(list)
    evidences_files = os.listdir(evidences_directory)
    for file_name in evidences_files:
        with open(evidences_directory + file_name, "r") as file:
            for line in file:
                evidence = json.loads(line)
                diseaseId, targetId, score = evidence["diseaseId"], evidence["targetId"], evidence["score"]
                evidences_data[f"{targetId}:{diseaseId}"].append(score)
    return evidences_data
import os
import json

def parse_targets_dataset():
    targets_directory = "targets/"
    targets_data = dict()
    targets_files = os.listdir(targets_directory)
    for file_name in targets_files:
        with open(targets_directory + file_name, "r") as file:
            for line in file:
                evidence = json.loads(line)
                id, approvedSymbol = evidence["id"], evidence["approvedSymbol"]
                targets_data[id] = approvedSymbol
    return targets_data

import os
import json

def parse_diseases_dataset():
    diseases_directory = "diseases/"
    diseases_data = dict()
    diseases_files = os.listdir(diseases_directory)
    for file_name in diseases_files:
        with open(diseases_directory + file_name, "r") as file:
            for line in file:
                evidence = json.loads(line)
                id, name = evidence["id"], evidence["name"]
                diseases_data[id] = name
    return diseases_data
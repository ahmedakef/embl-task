import json
from collections import defaultdict
from evidences import parse_evidences_dataset
from targets import parse_targets_dataset
from diseases import parse_diseases_dataset


def combine_all_data(evidences_data, targets_data, diseases_data):
    evidences_output = list()
    for key, scores in evidences_data.items():
        scores.sort()
        targetId, diseaseId = key.split(":")
        scores_len = len(scores)
        evidences_output.append({
            "diseaseId": diseaseId,
            "targetId": targetId,
            "median": scores[scores_len // 2],
            "top3": scores[-3:],
            "approvedSymbol": targets_data[targetId],
            "name": diseases_data[diseaseId]
        })
    evidences_output = sorted(evidences_output, key = lambda evidence: evidence["median"])
    return evidences_output


def calculate_matching_tagets(data):
    """
    This is the count of targets that causes two or more diseases
    which is different from the required count
    """
    diseases_to_tagets = defaultdict(set)
    for evidence in data:
        diseases_to_tagets[evidence["diseaseId"]].add(evidence["targetId"])

    total_matching_count = 0
    diseases_to_tagets_list = list(diseases_to_tagets.items())
    diseases_len = len(diseases_to_tagets_list)
    for i in range(diseases_len):
        for j in range(i+1, diseases_len):
            # intersections means that the same targets caused two diseases
            intersections = diseases_to_tagets_list[i][1].intersection(diseases_to_tagets_list[j][1])
            total_matching_count += len(intersections)
    return total_matching_count

def main():
    targets_data = parse_targets_dataset()
    diseases_data = parse_diseases_dataset()
    evidences_data = parse_evidences_dataset()
    data = combine_all_data(evidences_data, targets_data, diseases_data)
    json.dump(data, open("output.json", "w"))
    total_matching_count = calculate_matching_tagets(data)
    print(f"matching counts are {total_matching_count}")

if __name__ == "__main__":
    main()
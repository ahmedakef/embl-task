# EMBL: parse a large dataset
The goal of this problem is to access data via FTP, quickly and efficiently parse a large dataset,
and calculate statistics on the extracted data.
## running the project:
```bash
python3 etl.py
```